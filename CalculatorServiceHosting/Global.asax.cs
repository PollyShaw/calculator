﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Activation;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using CalculatorServiceImplementation;
using CalculatorServiceInterfaces;
using Castle.Facilities.WcfIntegration;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Castle.Windsor.Installer;

namespace CalculatorServiceHosting
{
    public class Global : System.Web.HttpApplication
    {
        private WindsorContainer _container;

        protected void Application_Start(object sender, EventArgs e)
        {

            _container = new WindsorContainer();
            _container.AddFacility<WcfFacility>();

            _container.Register(Component.For<ServiceHostFactoryBase>()
                            .ImplementedBy<DefaultServiceHostFactory>()
                            .LifestyleSingleton());
            // Register WCF services


            _container.Register(Component.For<IHcfService>()
                                         .ImplementedBy<HcfService>()
                                        
                                          .Named("HcfService")
                                          .LifestylePerWcfOperation());
                     
            _container.Register(Component.For<IPrimeTester>()
                                         .ImplementedBy<PrimeTester>()
                                         .Named("PrimeTester")
                                         .LifestylePerWcfOperation());

           
            // Register all other classes.
            _container.Register(Classes.FromAssemblyNamed("CalculatorServiceImplementation")
                                .InNamespace("CalculatorServiceImplementation")
                                .WithService
                                .DefaultInterfaces()
                                .LifestylePerWcfOperation());

        } 
    }
}