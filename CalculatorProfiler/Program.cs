﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using CalculatorServiceImplementation;
using CalculatorServiceImplementation.FourierTransformers;
using CalculatorServiceImplementation.PolynomialMultipliers;
using CalculatorServiceImplementation.ScalarMultipliers;

namespace CalculatorProfiler
{
    class Program
    {
        static void Main(string[] args)
        {
            var coefficients = new BigInteger[300];
            for (int i = 0; i < 300; i++)
            {
                coefficients[i] = i + 1;
            }

            var polynomial = new Polynomial(coefficients);
            
            var transformer = new FastFermatFourierTransformer(9);

            var multiplier = new FourierPolynomialMultiplier<BigInteger>(transformer, new BigIntegerScalarMultiplier());
            var now = DateTime.Now;
            /*for (int i = 0; i < 10; i++)
            {
                polynomial.Times(polynomial);
            }
            var naiveTimespan = DateTime.Now.Subtract(now); */
            now = DateTime.Now;
            for (int i = 0; i < 10; i++)
            {
                polynomial.Times(polynomial, multiplier);
            }
            var fourierTimespan = DateTime.Now.Subtract(now);
        }
    }
}
