﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using CalculatorServiceImplementation.FourierTransformers;
using CalculatorServiceImplementation.Interfaces;
using CalculatorServiceImplementation.ScalarMultipliers;

namespace CalculatorServiceImplementation.PolynomialMultipliers
{
    public class PolynomialMultplierFactory : IPolynomialMultiplierFactory
    {
        public IPolynomialMultiplier CreatePolynomialMultiplier(BigInteger numberBeingTested, long maxDegree)
        {
            if (maxDegree < 100)
            {
                return new NaivePolynomialMultiplier();
            }
            else if (numberBeingTested < 700000)
            {
                int lgMaxDegree = (int) Math.Ceiling(BigInteger.Log(maxDegree, 2));

                return new FourierPolynomialMultiplier<Complex>(
                    new FastComplexFourierTransformer(lgMaxDegree + 1),
                    new ComplexScalarMultiplier());
            }
            else
            {
                int lgMaxDegree = (int)Math.Ceiling(BigInteger.Log(maxDegree, 2));
                return new FourierPolynomialMultiplier<BigInteger>(
                    new FastFermatFourierTransformer(lgMaxDegree),
                    new BigIntegerScalarMultiplier());
            }
        }
    }
}
