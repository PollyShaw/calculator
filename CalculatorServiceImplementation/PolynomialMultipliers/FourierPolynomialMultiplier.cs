﻿using System.Numerics;
using CalculatorServiceImplementation.Interfaces;

namespace CalculatorServiceImplementation.PolynomialMultipliers
{
    public class FourierPolynomialMultiplier<T> : IPolynomialMultiplier
    {
        private readonly IFourierTransformer<T> _transformer;
        private readonly IScalarMultiplier<T> _scalarMultiplier;

        public FourierPolynomialMultiplier(IFourierTransformer<T> transformer, IScalarMultiplier<T> scalarMultiplier )
        {
            _transformer = transformer;
            _scalarMultiplier = scalarMultiplier;
        }

        public BigInteger[] Multiply(BigInteger[] a, BigInteger[] b)
        {
            var transformA = _transformer.Transform(a);
            var transformB = _transformer.Transform(b);

            var convolution = new T[transformA.Length];
            for (long i = 0; i < transformA.Length; i++)
            {
                convolution[i] = _scalarMultiplier.Times(ref transformA[i], ref transformB[i]);
            }

            return _transformer.InverseTransform(convolution);
        }

        public BigInteger[] Square(BigInteger[] a)
        {
            var transformA = _transformer.Transform(a);

            var convolution = new T[transformA.Length];
            for (long i = 0; i < transformA.Length; i++)
            {
                convolution[i] = _scalarMultiplier.Times(ref transformA[i], ref transformA[i]);
            }

            return _transformer.InverseTransform(convolution);
        }
    }


}
