﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using CalculatorServiceImplementation.Interfaces;

namespace CalculatorServiceImplementation.PolynomialMultipliers
{
    public class NaivePolynomialMultiplier : IPolynomialMultiplier
    {
        public BigInteger[] Multiply(BigInteger[] a, BigInteger[] b)
        {
            BigInteger[] resultCoefficients = new BigInteger[a.Length + b.Length -1];
            
            for (long i = 0; i < a.Length; i++)
            {
                for (long j = 0; j < b.Length; j++)
                {
                    resultCoefficients[i + j] +=  a[i] * b[j];
                }
            }

            return resultCoefficients;    
        }

        public BigInteger[] Square(BigInteger[] a)
        {
            return Multiply(a, a);
        }
    }
}
