﻿using System.Numerics;

namespace CalculatorServiceImplementation.Interfaces
{
    public interface IFourierTransformer<T>
    {
        T[] Transform(BigInteger[] start);
        BigInteger[] InverseTransform(T[] end);
    }
}
