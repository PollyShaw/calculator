﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorServiceImplementation.Interfaces
{
    public interface IPolynomialMultiplierFactory
    {
        IPolynomialMultiplier CreatePolynomialMultiplier(BigInteger numberBeingTested, long maxDegree);
    }
}
