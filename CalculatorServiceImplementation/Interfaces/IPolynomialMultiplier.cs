﻿using System.Numerics;

namespace CalculatorServiceImplementation.Interfaces
{
    public interface IPolynomialMultiplier
    {
        BigInteger[] Multiply(BigInteger[] a, BigInteger[] b);
        BigInteger[] Square(BigInteger[] a);
    }
}