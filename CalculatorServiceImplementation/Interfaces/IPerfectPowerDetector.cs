﻿using System.Numerics;

namespace CalculatorServiceImplementation.Interfaces
{
    public interface IPerfectPowerDetector
    {
        bool IsPerfectPower(BigInteger N, long maximumExponent);
    }
}