﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorServiceImplementation.Interfaces
{
    public interface IScalarMultiplier<T>
    {
        T Times(ref T a, ref T b);
    }
}
