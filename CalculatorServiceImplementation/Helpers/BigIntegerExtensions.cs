﻿using System;
using System.Numerics;

namespace CalculatorServiceImplementation.Helpers
{
    public static class BigIntegerExtensions
    {
        public static BigInteger SquareRoot(this BigInteger number)
        {
            if (number == BigInteger.Zero)
            {
                return BigInteger.Zero;
            }
            double bitLength = Math.Ceiling(BigInteger.Log(number, 2));
            int intBitLength = int.MaxValue;
            if (bitLength < int.MaxValue)
            {
                intBitLength = Convert.ToInt32(bitLength);
            }
            BigInteger lastGuess = 0;
            BigInteger guess = BigInteger.One << (intBitLength / 2);
            while (true)
            {

                BigInteger differenceSinceLastIteration = guess - lastGuess;

                lastGuess = guess;

                if (BigInteger.Abs(differenceSinceLastIteration) > BigInteger.One)
                {
                    // use Hero's method until the estimates start getting within one integer of each other
                    guess = (guess + number / guess)/2;
                }
                else
                {
                    // check the numbers either side 
                    var guessSquared = guess*guess;
                    if (guessSquared == number)
                    {
                        return guess;
                    }
                    if (guessSquared < number)
                    {
                        var guessPlusOne = guess + 1;
                        var guessPlusOneSquared = guessPlusOne*guessPlusOne;
                        if (guessPlusOneSquared == number)
                        {
                            return guessPlusOne;
                        }
                        else if (guessPlusOneSquared > number)
                        {
                            return guess;
                        }
                        else
                        {
                            guess = guessPlusOne;
                        }
                    }
                    else
                    {
                        var guessMinusOne = guess - 1;
                        var guessMinusOneSquared = guessMinusOne*guessMinusOne;

                        if (guessMinusOneSquared <= number)
                        {
                            return guessMinusOne;
                        }
                        else
                        {
                            guess = guessMinusOne;
                        }

                    }
                }
            }
        }

        public static BigInteger NthRoot(this BigInteger number, int exponent)
        {
            if (number == BigInteger.Zero)
            {
                return BigInteger.Zero;
            }
            double bitLength = Math.Ceiling(BigInteger.Log(number, 2));
            int intBitLength = int.MaxValue;
            if (bitLength < int.MaxValue)
            {
                intBitLength = Convert.ToInt32(bitLength);
            }
            BigInteger lastGuess = 0;
            BigInteger guess = BigInteger.One << (intBitLength / exponent);

            while (true)
            {

                BigInteger differenceSinceLastIteration = guess - lastGuess;

                lastGuess = guess;



                if (BigInteger.Abs(differenceSinceLastIteration) > BigInteger.One)
                {
                    // use Hero's method until the estimates start getting within one integer of each other
                    guess = (guess * (exponent - 1) + number / (BigInteger.Pow(guess, exponent - 1) )) / exponent;
                }
                else
                {
                    // check the numbers either side 
                    var guessToPower = BigInteger.Pow(guess, exponent);
                    if (guessToPower == number)
                    {
                        return guess;
                    }
                    if (guessToPower < number)
                    {
                        var guessPlusOne = guess + 1;
                        var guessPlusOneToPower = BigInteger.Pow(guessPlusOne, exponent);
                        if (guessPlusOneToPower == number)
                        {
                            return guessPlusOne;
                        }
                        else if (guessPlusOneToPower > number)
                        {
                            return guess;
                        }
                        else
                        {
                            guess = guessPlusOne;
                        }
                    }
                    else
                    {
                        var guessMinusOne = guess - 1;
                        var guessMinusOneToPower = BigInteger.Pow(guessMinusOne, exponent);

                        if (guessMinusOneToPower <= number)
                        {
                            return guessMinusOne;
                        }
                        else
                        {
                            guess = guessMinusOne;
                        }

                    }
                }
            }
        }
    }
}
