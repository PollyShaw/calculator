﻿using System;
using System.Numerics;
using CalculatorServiceImplementation.Interfaces;

namespace CalculatorServiceImplementation
{
    public class Polynomial
    {
        private readonly BigInteger[] _coefficients;
        public Polynomial(params BigInteger[] coefficients)
        {
        
            var indexOfHighestNonZeroCoefficient = coefficients.LongLength -1;
            while (indexOfHighestNonZeroCoefficient >= 0 && coefficients[indexOfHighestNonZeroCoefficient] == 0)
            {
                indexOfHighestNonZeroCoefficient--;
            }
            
            _coefficients = new BigInteger[indexOfHighestNonZeroCoefficient + 1];
            
            for (long i = 0; i <= indexOfHighestNonZeroCoefficient; i++)
            {
                _coefficients[i] = coefficients[i];
            }
        }

        private Polynomial()
        {
            _coefficients = new BigInteger[0];
        }

        public static Polynomial Zero()
        {
            return new Polynomial();
        } 

        public long Order()
        {
            return Math.Max(_coefficients.LongLength - 1, 0);
        }

        public BigInteger CoefficientAt(long coefficientIndex)
        {
            if (coefficientIndex < _coefficients.LongLength)
            {
                return _coefficients[coefficientIndex];
            }
            else
            {
                return BigInteger.Zero;
            }
        }

        public Polynomial ReduceMod(BigInteger divisor, long polyDegree)
        {

            BigInteger[] resultCoefficients = new BigInteger[polyDegree];
            for (long i = Order(); i >= 0; i--)
            {
                resultCoefficients[i%polyDegree] = (resultCoefficients[i%polyDegree] + _coefficients[i]) % divisor;
            }
            return new Polynomial(resultCoefficients);
            
        }

        public Polynomial ReduceMod(BigInteger divisor)
        {

            BigInteger[] resultCoefficients = new BigInteger[Order() + 1];
            _coefficients.CopyTo(resultCoefficients, 0);
            for (long i = Order(); i >= 0; i--)
            {
                resultCoefficients[i] = BigInteger.Remainder(resultCoefficients[i], divisor);
            }
            return new Polynomial(resultCoefficients);
        }

        public Polynomial ReduceMod(Polynomial divisor)
        {
            if (divisor.CoefficientAt(divisor.Order()) != BigInteger.One)
            {
                throw new ArgumentException("The leading coefficient of the divisor must be one", "divisor");
            }
            BigInteger[] resultCoefficients = new BigInteger[Order() + 1];
            _coefficients.CopyTo(resultCoefficients, 0);
            long order = Order();
            while (order >= divisor.Order())
            {
                BigInteger highestCoefficient = resultCoefficients[order];
                for (long i = 0; i <= divisor.Order(); i++)
                {
                    resultCoefficients[order - i] = resultCoefficients[order - i] -
                                               BigInteger.Multiply(divisor.CoefficientAt(divisor.Order() - i),
                                                                   highestCoefficient);
                }
                while (resultCoefficients[order] == BigInteger.Zero && order > 0)
                {
                    order--;
                } 
            }
            return new Polynomial(resultCoefficients);
            
        }

        public Polynomial Times(Polynomial otherFactor, IPolynomialMultiplier multiplier)
        {
            if (this.IsZero() || otherFactor.IsZero())
            {
                return Polynomial.Zero();
            }

            BigInteger[] resultCoefficients = multiplier.Multiply(this._coefficients, otherFactor._coefficients);

            return new Polynomial(resultCoefficients);
            

        }

        public Polynomial Times(Polynomial otherFactor)
        {
            BigInteger[] resultCoefficients = new BigInteger[Order() + otherFactor.Order() + 1];
            if (this.IsZero() || otherFactor.IsZero()) 
            {
                return Polynomial.Zero();
            }

            for (long i = 0; i <= otherFactor.Order(); i++)
            {
                for (long j = 0; j <= this.Order(); j++)
                {
                    resultCoefficients[i + j] = BigInteger.Add(
                        resultCoefficients[i + j], 
                        BigInteger.Multiply(this.CoefficientAt(j), otherFactor.CoefficientAt(i)));
                }
            }

            return new Polynomial(resultCoefficients);            
        }

        /// <summary>
        /// Returns the current polynomial multipled by another polynomial 
        /// reduced mod X^r - 1 and N
        /// </summary>
        /// <param name="multiplier">Polynomial this is multipled by</param>
        /// <param name="r">Exponent of the polynomial X^r - 1 that the result is reduced mod</param>
        /// <param name="N">Integer the polynomial is reduced by</param>
        /// <returns></returns>
        public Polynomial TimesMod(Polynomial multiplier, long r, BigInteger N)
        {
            BigInteger[] resultCoefficients = new BigInteger[Math.Min(r, this.Order() + multiplier.Order() + 1)];
            if (this.IsZero() || multiplier.IsZero())
            {
                return Polynomial.Zero();
            }

            for (long i = 0; i <= multiplier.Order(); i++)
            {
                for (long j = 0; j <= this.Order(); j++)
                {
                    long accumulatingCoefficient = (i + j)%r;
                    resultCoefficients[accumulatingCoefficient] = (resultCoefficients[accumulatingCoefficient] + CoefficientAt(j) * multiplier.CoefficientAt(i)) % N;
                }
            }

            
            return new Polynomial(resultCoefficients);
        }


        public Boolean IsZero()
        {
            return this.Order() == 0 && this.CoefficientAt(0) == BigInteger.Zero;
        }

        public Polynomial Square(IPolynomialMultiplier multiplier)
        {
            if (this.IsZero() )
            {
                return Polynomial.Zero();
            }

            BigInteger[] resultCoefficients = multiplier.Square(this._coefficients);

            
            return new Polynomial(resultCoefficients);
            

        }
    }
}

    