﻿using System;
using System.Numerics;
using CalculatorServiceImplementation.Interfaces;
using CalculatorServiceInterfaces;

namespace CalculatorServiceImplementation.FourierTransformers
{
    public class FastFermatFourierTransformer : IFourierTransformer<BigInteger>
    {
        private readonly BigInteger _mod;
        private readonly int _power;
        private readonly BigInteger _inverseOfPower;
        private readonly BigInteger _modMinus2;
        private readonly BigInteger _modTimes2;
        private readonly BigInteger[] SingleZeroArray = new[]{BigInteger.Zero};

        public FastFermatFourierTransformer(int fermatIndex)
        {

            
            _power = 1 << (fermatIndex + 1);
            _mod = (BigInteger.One << (_power/2)) + 1;
            _modMinus2 = _mod - 2;
            _modTimes2 = _mod << 1;
            _inverseOfPower = BigInteger.ModPow((_mod + 1)/2, fermatIndex + 1, _mod);
            

            if ((_inverseOfPower * _power) % _mod != BigInteger.One)
            {
                throw new ApplicationException(String.Format("{0} is not the inverse of {1} mod {2}", _power, _inverseOfPower, _mod));
            }            
        }


        public BigInteger[] Transform(BigInteger[] start)
        {
            long outputArrayStartIndex;
            return Transform(start, out outputArrayStartIndex, inverse: false);
        }

        public BigInteger[] InverseTransform(BigInteger[] end)
        {
            long outputArrayStartIndex;
            return Transform(end, out outputArrayStartIndex, inverse: true);
        }

        private BigInteger[] Transform(BigInteger[] start, out long outputArrayStartIndex , bool inverse = false, long step = 1, long startIndex = 0)
        {
            outputArrayStartIndex = 0;
            long effectivePower = _power / step;
            
            if (effectivePower == 1)
            {
                if (startIndex >= start.Length)
                {
                    return SingleZeroArray;
                }
                outputArrayStartIndex = startIndex;
                return start;
                
            }
            

            long effectivePowerOver2 = effectivePower / 2;


            long evenStartIndex;
            long oddStartIndex;
            var evenTransform = Transform(start, out evenStartIndex, inverse: inverse, step: step * 2, startIndex: startIndex);
            var oddTransform = Transform(start, out oddStartIndex, inverse: inverse, step: step * 2, startIndex: startIndex + step);


            BigInteger[] result = new BigInteger[effectivePower];
            for (long i = 0, currentPowerIndex = 0; i < effectivePowerOver2; i++, currentPowerIndex += step)
            {
                long currentPowerIndexAdjustedForInverse = 0;
                if (currentPowerIndex != 0)
                {
                    currentPowerIndexAdjustedForInverse = inverse ? _power - currentPowerIndex : currentPowerIndex;
                }
                
                long backwardsI = i + effectivePowerOver2;
                if (oddTransform[i + oddStartIndex] != 0)
                {
                    BigInteger twiddledOdd;
                    
                    if (currentPowerIndexAdjustedForInverse < _power / 2)
                    {
                        twiddledOdd = (oddTransform[i + oddStartIndex] << (int)currentPowerIndexAdjustedForInverse);
                        ReduceMod(ref twiddledOdd);
                    }
                    else
                    {
                        twiddledOdd = -(oddTransform[i + oddStartIndex] << (int)(currentPowerIndexAdjustedForInverse % (_power / 2)));
                        ReduceMod(ref twiddledOdd);
                            
                    }
                    
                    
                    result[i] = evenTransform[i + evenStartIndex] + twiddledOdd;
                    ReduceMod(ref result[i]);
                    result[backwardsI] = evenTransform[i + evenStartIndex] - twiddledOdd;
                    ReduceMod(ref result[backwardsI]);

                }
                else
                {
                    result[i] = evenTransform[i + evenStartIndex];
                    result[backwardsI] = evenTransform[i + evenStartIndex];

                }


                if (inverse && step == 1)
                {
                    if (result[i] != 0)
                    {
                        result[i] = result[i]*_inverseOfPower;
                        ReduceMod(ref result[i]);
                    }
                    if (result[backwardsI] != 0)
                    {
                        result[backwardsI] = result[backwardsI]*_inverseOfPower;
                        ReduceMod(ref result[backwardsI]);
                    }
                }

            }


            return result;
        }

        
        private void ReduceMod(ref BigInteger numberToReduce)
        {
            if (numberToReduce > 0 && numberToReduce < _mod)
            {
                return;
            }
            
            if (numberToReduce >= 0)
            {
                if (numberToReduce < _mod)
                {
                    return;
                }
                if (numberToReduce < _modTimes2)
                {
                    numberToReduce = numberToReduce - _mod;
                    return;
                }
            }

            else if (numberToReduce >= -_mod)
            {
                numberToReduce = numberToReduce + _mod;
                return;
            }
            
            bool signFlipped = false;
            var absoluteNumberToReduce = numberToReduce;
            if (numberToReduce < 0)
            {
                absoluteNumberToReduce = -numberToReduce;
                signFlipped = true;
            }
            numberToReduce = absoluteNumberToReduce;
            if (absoluteNumberToReduce >= _mod)
            {


                BigInteger rightHandSide = _modMinus2 & absoluteNumberToReduce;
                BigInteger leftHandSideRightShifted = absoluteNumberToReduce >> (_power/2);
                numberToReduce = rightHandSide - leftHandSideRightShifted;
            }

            if (signFlipped)
            {
                numberToReduce = -numberToReduce;
            }
            if (numberToReduce < 0)
            {
                numberToReduce = numberToReduce + _mod;

                if (numberToReduce < 0)
                {
                    numberToReduce = numberToReduce % _mod;
                    if (numberToReduce < 0)
                    {
                        numberToReduce = numberToReduce + _mod;
                    }
                }
            }
            if (numberToReduce > _mod)
            {
                numberToReduce = numberToReduce % _mod;
            }
        }
    }
}