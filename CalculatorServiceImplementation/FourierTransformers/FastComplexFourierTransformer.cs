﻿using System;
using System.Linq;
using System.Numerics;
using CalculatorServiceImplementation.Exceptions;
using CalculatorServiceImplementation.Interfaces;
using CalculatorServiceInterfaces;

namespace CalculatorServiceImplementation.FourierTransformers
{

    public class FastComplexFourierTransformer : IFourierTransformer<Complex>
    {
        private readonly int _power;
        private readonly Complex[] _powers;
        private readonly Complex[] SingleZeroArray = new[]{Complex.Zero};
        
        public FastComplexFourierTransformer(int exponentOfPower)
        {

            
            _power = 1 << exponentOfPower;
            _powers = new Complex[_power];
            var baseAngle = 2 * Math.PI/_power;
            for (int i =0; i <_power; i++)
            {
                _powers[i] = new Complex(Math.Cos(baseAngle * i), Math.Sin(baseAngle * i));
            }
        }


        public Complex[] Transform(BigInteger[] start)
        {
            var complexArray = start.Select(i => new Complex((double) i, 0)).ToArray();
            long outputArrayStartIndex;
            return Transform(complexArray, out outputArrayStartIndex, inverse: false);
        }

        public BigInteger[] InverseTransform(Complex[] end)
        {
            long outputArrayStartIndex;
            return Transform(end, out outputArrayStartIndex, inverse: true)
                .Select( c=> {
                                if (Math.Abs(c.Imaginary) > 0.1)
                                    {
                    
                                        throw new FastComplexFourierTransformerException(
                                            "Imaginary part of number greater than 0.1");
                    
                                    }
                                return new BigInteger(Math.Round(c.Real));
                })
                .ToArray();
        }

        private Complex[] Transform(Complex[] start, out long outputArrayStartIndex , bool inverse = false, long step = 1, long startIndex = 0)
        {
            outputArrayStartIndex = 0;
            long effectivePower = _power / step;
            
            if (effectivePower == 1)
            {
                if (startIndex >= start.Length)
                {
                    return SingleZeroArray;
                }
                outputArrayStartIndex = startIndex;
                return start;
                
            }
            

            long effectivePowerOver2 = effectivePower / 2;


            long evenStartIndex;
            long oddStartIndex;
            var evenTransform = Transform(start, out evenStartIndex, inverse: inverse, step: step * 2, startIndex: startIndex);
            var oddTransform = Transform(start, out oddStartIndex, inverse: inverse, step: step * 2, startIndex: startIndex + step);


            Complex[] result = new Complex[effectivePower];
            for (long i = 0, currentPowerIndex = 0; i < effectivePowerOver2; i++, currentPowerIndex += step)
            {
                long currentPowerIndexAdjustedForInverse = 0;
                if (currentPowerIndex != 0)
                {
                    currentPowerIndexAdjustedForInverse = inverse ? _power - currentPowerIndex : currentPowerIndex;
                }
                
                long backwardsI = i + effectivePowerOver2;
                if (oddTransform[i + oddStartIndex] != 0)
                {
                    Complex twiddledOdd =
                        oddTransform[i + oddStartIndex]*_powers[currentPowerIndexAdjustedForInverse];
                    
                    
                    result[i] = evenTransform[i + evenStartIndex] + twiddledOdd;
                    result[backwardsI] = evenTransform[i + evenStartIndex] - twiddledOdd;
                    
                }
                else
                {
                    result[i] = evenTransform[i + evenStartIndex];
                    result[backwardsI] = evenTransform[i + evenStartIndex];

                }


                if (inverse && step == 1)
                {
                    if (result[i] != 0)
                    {
                        result[i] = result[i]/ _power;
                    }
                    if (result[backwardsI] != 0)
                    {
                        result[backwardsI] = result[backwardsI] /_power;
                       
                    }
                }

            }


            return result;
        }

        
   }
}