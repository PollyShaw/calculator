﻿using System;
using System.Numerics;
using CalculatorServiceImplementation.Interfaces;
using CalculatorServiceInterfaces;

namespace CalculatorServiceImplementation.FourierTransformers
{
    public class SlowFourierTransformer : IFourierTransformer<BigInteger>
    {
        private readonly BigInteger _mod;
        private readonly long _power;
        private readonly BigInteger _inverseOfPower;
        private readonly BigInteger[] _powersOfRoot;
        
        public SlowFourierTransformer(BigInteger mod, BigInteger root, long power, BigInteger inverseOfPower)
        {
            _mod = mod;
           
            _power = power;
            _inverseOfPower = inverseOfPower;
            _powersOfRoot = new BigInteger[power];
            _powersOfRoot[0] = 1;
            _powersOfRoot[1] = root;
            BigInteger currentPowerOfRoot = root;
            for (long i = 2; i < power; i++)
            {
                currentPowerOfRoot = (currentPowerOfRoot * root) % mod;

                _powersOfRoot[i] = currentPowerOfRoot;
            }

            if ((currentPowerOfRoot * root) % mod != BigInteger.One)
            {
                throw new ApplicationException(String.Format("{0} is not a {1}th root of unity mod {2}", root, power, mod));
            }

            if ((inverseOfPower * power) % mod != BigInteger.One)
            {
                throw new ApplicationException(String.Format("{0} is not the inverse of {1} mod {2}", power, inverseOfPower, mod));
            }

            
        }

        public BigInteger[] Transform(BigInteger[] start, bool inverse)
        {
            BigInteger[] results = new BigInteger[_power];
            for (long i = 0; i < _power; i++)
            {
                var baseRootOfThisStep = inverse ? _power - i : i;
                for (long j = 0; j < _power; j ++)
                {
                    results[i] = (results[i] + _powersOfRoot[(baseRootOfThisStep * j) % _power] * (start.Length > j ? start[j] : 0)) % _mod;
                }
                if (inverse)
                {
                    results[i] = results[i] * _inverseOfPower % _mod;
                }
            }

            return results;
        }

        public BigInteger[] InverseTransform(BigInteger[] end)
        {
            return Transform(end, true);
        }

        public BigInteger[] Transform(BigInteger[] start)
        {
            return Transform(start, false);
        }

       
    }
}
