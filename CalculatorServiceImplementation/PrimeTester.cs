﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using CalculatorServiceImplementation.Helpers;
using CalculatorServiceImplementation.Interfaces;
using CalculatorServiceImplementation.PolynomialMultipliers;
using CalculatorServiceInterfaces;

namespace CalculatorServiceImplementation
{
    public class PrimeTester : IPrimeTester
    {
        private readonly PerfectPowerDetector _perfectPowerDetector;
        private readonly IPolynomialMultiplierFactory _polynomialMultiplierFactory;

        public PrimeTester(PerfectPowerDetector perfectPowerDetector, IPolynomialMultiplierFactory polynomialMultiplierFactory )
        {
            _perfectPowerDetector = perfectPowerDetector;
            _polynomialMultiplierFactory = polynomialMultiplierFactory;
        }

        public PrimeTester()
        {
            _perfectPowerDetector = new PerfectPowerDetector();
            _polynomialMultiplierFactory = new PolynomialMultplierFactory();
        }

        /// <summary>
        /// Uses the AKS algorithm to determine whether a number is prime
        /// </summary>
        /// <param name="number">Number whose primality is to be determined.</param>
        /// <returns></returns>
        public bool IsPrime(string number)
        {
            BigInteger N = BigInteger.Parse(number);
            if (N < 3)
            {
                throw new ArgumentException("Number must be greater than 2", "number");
            }

            double logOfN = BigInteger.Log(N, 2);
            BigInteger logOfNSquared = new BigInteger(Math.Ceiling(logOfN * logOfN));

            BigInteger r;
            try
            {
                r = GetLowestBaseWhoseOrderIsGreaterThanLogOfNSquared(logOfNSquared, N);
            }
            catch (IsPrimeException)
            {
                return true;
            }
            catch (IsCompositeException)
            {
                return false;
            }


            // Find out whether it's a perfect power 
            if (_perfectPowerDetector.IsPerfectPower(N,
                                                     (long)Math.Floor(BigInteger.Log(N) / BigInteger.Log(r))))
            {
                return false;
            }

            var maxValueOfA = (logOfNSquared * r).SquareRoot();

            var binaryRepresentationOfNumber = BinaryRepresentationOfNumber(N);

          
            var multiplier = _polynomialMultiplierFactory.CreatePolynomialMultiplier(N, (long)r);
            for (BigInteger a = 1; a <= maxValueOfA; a++)
            {
                
                var accumulator = GetXPlusAToThePowerOfN(a, binaryRepresentationOfNumber, r, N, multiplier);

                if (!ResultIsEqualToXToTheNPlusA(N, r, accumulator, a)) return false;
            }
            return true;
        }

        /// <summary>
        /// This method determines whether a polynomial is equal to X^N + a mod N and (X^R -1)
        /// </summary>
        /// <param name="N"></param>
        /// <param name="r"></param>
        /// <param name="accumulator"></param>
        /// <param name="a"></param>
        /// <returns></returns>
        private static bool ResultIsEqualToXToTheNPlusA(BigInteger N, BigInteger r, Polynomial accumulator, BigInteger a)
        {
            var nModR = N % r;

            if (accumulator.CoefficientAt(0) != a)
            {
                return false;
            }
            if (accumulator.CoefficientAt((long)nModR) != 1)
            {
                return false;
            }

            for (long coefficientIndex = 1; coefficientIndex < nModR; coefficientIndex++)
            {
                if (accumulator.CoefficientAt(coefficientIndex) != 0)
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// This method calculates (X+a)^N mod N and (X^r -1), where X is a formal variable.
        /// </summary>
        /// <param name="a">Second part of the binomial</param>
        /// <param name="binaryRepresentationOfNumber">A binary representation of the number, 
        ///     with the bits to be dequeued first being the least significant</param>
        /// <param name="r">Exponent of X^r which is part of the polynomial to reduce the result by</param>
        /// <param name="N">Exponent of the binomial and the number to reduce the result mod.</param>
        /// <param name="transformer"></param>
        /// <returns></returns>
        private static Polynomial GetXPlusAToThePowerOfN(BigInteger a, Queue<bool> binaryRepresentationOfNumber, BigInteger r, BigInteger N, IPolynomialMultiplier multiplier)
        {
            
            var currentPower = new Polynomial(new BigInteger[] { a, 1 });
            var accumulator = new Polynomial(new BigInteger[] { 1 });
            
            // We use repeated squares to find (X + a)^N
            foreach (bool bit in binaryRepresentationOfNumber)
            {
                if (bit)
                {
                    if (accumulator.Order() > 100)
                    {
                        accumulator = accumulator.Times(currentPower, multiplier);
                        accumulator = accumulator.ReduceMod(N, (long) r);
                    }
                    else
                    {
                        accumulator = accumulator.TimesMod(currentPower, (long)r, N);
                    }

                }

                if (currentPower.Order() > 100)
                {
                    currentPower = currentPower.Square(multiplier);
                    currentPower = currentPower.ReduceMod(N, (long)r);
                }
                else
                {
                    currentPower = currentPower.TimesMod(currentPower, (long)r, N);
                }
                    
               
            }
            return accumulator;
        }

        /// <summary>
        /// Finds the lowest integer which N has an order > orderSought in Z/rZ
        /// Throws an IsPrimeException if orderSought > N and there are no factors of N less than orderSought, 
        /// and an IsComposite exception if a number less than orderSought divides N. 
        /// </summary>
        /// <param name="orderSought"> The order that we want to find N greater than in</param>
        /// <param name="N"></param>
        /// <returns></returns>
        private static BigInteger GetLowestBaseWhoseOrderIsGreaterThanLogOfNSquared(BigInteger orderSought, BigInteger N)
        {


            BigInteger r;
            for (r = 2; r < N; r++)
            {
                // Up to orderSought we don't bother computing powers
                // because the order will certainly be less than orderSought.

                // If we've got up to N without finding any factors then we can conclude that N is prime
                if (r == N)
                {
                    throw new IsPrimeException();
                }

                // if r divides N then we can conclude that N is composite
                if (N % r == 0)
                {
                    throw new IsCompositeException();
                }

                if (r >= orderSought)
                {
                    BigInteger orderOfR = BigInteger.One;
                    BigInteger remainderAfterMultiplication = N;
                    do
                    {
                        orderOfR++;
                        remainderAfterMultiplication = remainderAfterMultiplication * N % r;
                    } while (remainderAfterMultiplication != BigInteger.One && orderOfR < orderSought);
                    if (orderOfR >= orderSought)
                    {
                        return r;
                    }
                }
            }
            throw new IsPrimeException();
        }

        // This produces a queue with the binary representation of a number that we use when 
        // computing the Nth power of a binomial.
        private static Queue<bool> BinaryRepresentationOfNumber(BigInteger integerNumber)
        {
            Queue<bool> binaryRepresentationOfNumber = new Queue<bool>();
            var numberShiftedRight = integerNumber;
            while (numberShiftedRight != 0)
            {
                BigInteger thisBit;
                numberShiftedRight = BigInteger.DivRem(numberShiftedRight, 2, out thisBit);
                binaryRepresentationOfNumber.Enqueue(thisBit == BigInteger.One);
            }
            return binaryRepresentationOfNumber;
        }
    }
}

internal class IsCompositeException : ApplicationException
{
}

internal class IsPrimeException : ApplicationException
{
}