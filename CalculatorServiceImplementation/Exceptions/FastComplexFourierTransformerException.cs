﻿using System;

namespace CalculatorServiceImplementation.Exceptions
{
    public class FastComplexFourierTransformerException : ApplicationException
    {
        public FastComplexFourierTransformerException() : base()
        {
        }

        public FastComplexFourierTransformerException(String message) : base(message) { }

        public FastComplexFourierTransformerException(String message, Exception innerException) :
            base(message, innerException)
        {
        }
    }
}
