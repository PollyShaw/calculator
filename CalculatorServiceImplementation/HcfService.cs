﻿using System;
using CalculatorServiceInterfaces;

namespace CalculatorServiceImplementation
{
    public class HcfService : IHcfService
    {
        // Implements Euclid's algorithm for Highest Common Factor
        public int GetHcf(int a, int b)
        {
            // Make sure both arguments are non-negative
            a = Math.Abs(a);
            b = Math.Abs(b);
            if (a == 0) {
                return b; 
            }
            if (b == 0)
            {
                return a;
            }
            if (b > a)
            {
                return GetHcf(a, b % a);
            }
            return GetHcf(a % b, b);
        }
    }
}
