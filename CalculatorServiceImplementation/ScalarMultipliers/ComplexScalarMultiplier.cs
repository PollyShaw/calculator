﻿using System.Numerics;
using CalculatorServiceImplementation.Interfaces;

namespace CalculatorServiceImplementation.ScalarMultipliers
{
    public class ComplexScalarMultiplier : IScalarMultiplier<Complex>
    {
        public Complex Times(ref Complex a, ref Complex b)
        {
            return a * b;
        }
    }
}
