﻿using System.Numerics;
using CalculatorServiceImplementation.Interfaces;

namespace CalculatorServiceImplementation.ScalarMultipliers
{
    public class BigIntegerScalarMultiplier : IScalarMultiplier<BigInteger>
    {
        public BigInteger Times(ref BigInteger a, ref BigInteger b)
        {
            return a * b;
        }
    }
}
