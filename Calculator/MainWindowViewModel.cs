﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using CalculatorServiceInterfaces;
using Caliburn.Micro;

namespace Calculator
{
    public class MainWindowViewModel : PropertyChangedBase, IShell
    {
        private string _a;
        private string _b;
        private string _resultOfCalculatingHcf;
        private string _numberForPrimalityTesting;
        private string _resultOfPrimalityTesting;

        private  IHcfService _hcfService;
        private IPrimeTester _primeTester;
        
        public MainWindowViewModel(IHcfService hcfService, IPrimeTester primeTester) {
            _hcfService = hcfService;
            _primeTester = primeTester;
        }
  
        public string A {
            get 
            { 
                return _a;
            }
            set 
            { 
                _a = value;
                NotifyOfPropertyChange(() => A);
                NotifyOfPropertyChange(() => CanCalculateHcf);
            }
        }

        public string B {
            get 
            { 
                return _b;
            }
            set 
            { 
                _b = value;
                NotifyOfPropertyChange(() => B);
                NotifyOfPropertyChange(() => CanCalculateHcf);
            }
        }

        public string NumberForPrimalityTesting
        {
            get 
            {
                return _numberForPrimalityTesting;
            }
            set 
            { 
                _numberForPrimalityTesting = value;
                NotifyOfPropertyChange(() => NumberForPrimalityTesting);
                NotifyOfPropertyChange(() => CanTestPrimality);
            }
        }


        public string ResultOfCalculatingHcf {
            get 
            { 
                return _resultOfCalculatingHcf;
            }
            set 
            { 
                _resultOfCalculatingHcf = value;
                NotifyOfPropertyChange(() => ResultOfCalculatingHcf);
            }
        }

        public string ResultOfPrimalityTesting
        {
            get
            {
                return _resultOfPrimalityTesting;
            }
            set
            {
                _resultOfPrimalityTesting = value;
                NotifyOfPropertyChange(() => ResultOfPrimalityTesting);
            }
        }

        public Boolean CanCalculateHcf
        {
            get 
            {
                int a;
                int b;
                return int.TryParse(_a, out a) && int.TryParse(_b, out b);
            }
        }

        public Boolean CanTestPrimality
        {
            get
            {
                BigInteger N;
                return BigInteger.TryParse(_numberForPrimalityTesting, out N);
            }
        }



        public void CalculateHcf()
        {
            int a;
            int b;
            a = int.Parse(A);
            b = int.Parse(B);
            ResultOfCalculatingHcf = String.Format("The HCF of {0} and {1} is {2}", a, b,_hcfService.GetHcf(a, b).ToString());
        }

        public void TestPrimality()
        {
            ResultOfPrimalityTesting = String.Format("{0} is{1} prime", NumberForPrimalityTesting,
                _primeTester.IsPrime(NumberForPrimalityTesting)? "": " not");
        }

    }
}
