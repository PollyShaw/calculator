﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using CalculatorServiceInterfaces;
using Caliburn.Micro;
using Castle.Facilities.WcfIntegration;
using Castle.MicroKernel.Registration;
using Castle.Windsor;

namespace Calculator
{
    public class WindsorBootstrapper : Bootstrapper<IShell>
    {
        private WindsorContainer _container;

        protected override void Configure()
        {
            _container = new WindsorContainer();
            _container.AddFacility<WcfFacility>();
            _container.Register(
                Component.For<IWindowManager>()
                        .ImplementedBy<WindowManager>().LifestyleSingleton(),
                Component.For<IEventAggregator>()
                        .ImplementedBy<EventAggregator>().LifestyleSingleton(),
                Component.For<IShell>()
                        .ImplementedBy<MainWindowViewModel>().LifestyleSingleton(),
                Component.For<IHcfService>()
                        .AsWcfClient(new DefaultClientModel(WcfEndpoint.
                                BoundTo(new BasicHttpBinding("defaultBinding"))
                        .At("http://localhost:1274/Hcfservice.svc"))),
                Component.For<IPrimeTester>()
                        .AsWcfClient(new DefaultClientModel(WcfEndpoint.
                                BoundTo(new BasicHttpBinding("defaultBinding"))
                        .At("http://localhost:1274/PrimeTester.svc")))
                );
        }

        protected override object GetInstance(Type serviceType, string key)
        {
            if (String.IsNullOrWhiteSpace(key)) {
                return _container.Resolve(serviceType);
            }
            return _container.Resolve(serviceType);
        }

        protected override IEnumerable<object> GetAllInstances(Type serviceType)
        {
            return from object instance in _container.ResolveAll(serviceType) select instance;
        }

        protected override void BuildUp(object instance) 
        {
            base.BuildUp(instance);
        }  
    }
}
