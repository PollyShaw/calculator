﻿using System.ServiceModel;

namespace CalculatorServiceInterfaces
{
    [ServiceContract]
    public interface IHcfService
    {
        [OperationContract]
        int GetHcf(int a, int b);
    }
}
