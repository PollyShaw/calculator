﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorServiceInterfaces
{
    public class PerfectPowerDetector : IPerfectPowerDetector
    {
        public bool IsPerfectPower(BigInteger N, long maximumExponent)
        {
            // We only need to test the prime roots so create a seive for the exponents
            bool[] isComposite = new bool[maximumExponent + 1];

            var currentRoot = 2;

            while (currentRoot < maximumExponent)
            {
                if (isComposite[currentRoot])
                {
                    currentRoot++;
                    if (currentRoot == maximumExponent)
                    {
                        break;
                    }
                }
                for(var currentIndex = currentRoot*currentRoot; currentIndex < maximumExponent; currentIndex += currentRoot)
                {
                    isComposite[currentIndex] = true;
                }


            }

            throw new NotImplementedException();

        }
    }
}
