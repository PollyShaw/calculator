﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorServiceInterfaces
{
    [ServiceContract]
    public interface IPrimeTester
    {
        [OperationContract]
        bool IsPrime(String number); 
    }
}
