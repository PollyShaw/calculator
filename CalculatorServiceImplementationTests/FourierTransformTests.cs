﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using CalculatorServiceImplementation;
using CalculatorServiceImplementation.Exceptions;
using CalculatorServiceImplementation.FourierTransformers;
using NUnit.Framework;

namespace CalculatorServiceImplementationTests
{
    [TestFixture]
    public class FourierTransformTests
    {
        [Test]
        public void InvertOnePlusOne()
        {
            var transformer = new SlowFourierTransformer(3,2,2,2);
            var transform =  transformer.Transform(new[] {BigInteger.One, 1});
            var inverseTransform = transformer.InverseTransform(transform);

            Assert.AreEqual(BigInteger.One, inverseTransform[0]);
            Assert.AreEqual(BigInteger.One, inverseTransform[1]);
        }


        [Test]
        public void InvertOnePlusThreePlusOne()
        {
            var transformer = new SlowFourierTransformer(5, 2, 4, 4);
            var transform = transformer.Transform(new[] { BigInteger.One, 3, 1 });
            var inverseTransform = transformer.InverseTransform(transform);

            Assert.AreEqual(BigInteger.One, inverseTransform[0]);
            Assert.AreEqual(new BigInteger(3), inverseTransform[1]);
            Assert.AreEqual(new BigInteger(1), inverseTransform[2]);
            Assert.AreEqual(BigInteger.Zero, inverseTransform[3]);
        }


        [Test]
        public void TestFastTransformIsSameAsNormalOne()
        {
            var slowTransformer = new SlowFourierTransformer(5, 2, 4, 4);
            var fastTransformer = new FastFermatFourierTransformer(1);
            var polyToTransform = new[] {BigInteger.One, 3, 1};
            var transform = slowTransformer.Transform(polyToTransform);
            var fastTransform = fastTransformer.Transform(polyToTransform);

            CollectionAssert.AreEqual(transform, fastTransform);
        }


        [Test]
        public void TestFastInverseTransformIsSameAsNormalOne()
        {
            var slowTransformer = new SlowFourierTransformer(5, 2, 4, 4);
            var fastTransformer = new FastFermatFourierTransformer(1);
            var polyToTransform = new[] { BigInteger.One, 3, 1 };
            var transform = slowTransformer.InverseTransform(polyToTransform);
            var fastTransform = fastTransformer.InverseTransform(polyToTransform);

            CollectionAssert.AreEqual(transform, fastTransform);
        }


        [Test]
        public void TestMultplyingUsingComplexFourierTransformIsSameAsNormal()
        {
            var fastTransformer = new FastComplexFourierTransformer(4);
            var polyToTransform = new[] {BigInteger.One, 3, 1};
            var transform = fastTransformer.Transform(polyToTransform);
            for (int i = 0; i < 16; i++)
            {
                transform[i] = transform[i]*transform[i];
            }
            var unTransform = fastTransformer.InverseTransform(transform);

            var polySquared = new Polynomial(polyToTransform).Times(new Polynomial(polyToTransform));

            for (int i = 0; i <= polySquared.Order(); i++)
            {
                Assert.AreEqual(polySquared.CoefficientAt(i), unTransform[i]);
            }

        }

        [Test]
        public void TestComplexTransformOfOneIsOne()
        {
            var fastTransformer = new FastComplexFourierTransformer(4);
            var polyToTransform = new[] {BigInteger.One};
            var transform = fastTransformer.Transform(polyToTransform);
            for (int i = 0; i < 16; i++)
            {
                Assert.AreEqual(transform[i], Complex.One);
            }
        }

        [Test]
        public void TestComplexTransformOfXIsPowersOfOmega()
        {
            var fastTransformer = new FastComplexFourierTransformer(4);
            var polyToTransform = new[] { 0, BigInteger.One };
            var transform = fastTransformer.Transform(polyToTransform);
            var baseAngle = 2 * Math.PI/16;
            for (int i = 0; i < 16; i++)
            {
                Assert.True((transform[i] - new Complex(Math.Cos(i* baseAngle), Math.Sin(i * baseAngle))).Magnitude < 0.000001);
            }
        }

        [Test]
        public void InvertOnePlusThreePlusOneUsingFFT()
        {
            var transformer = new FastFermatFourierTransformer(1);
            var transform = transformer.Transform(new[] { BigInteger.One, 3, 1 });
            var inverseTransform = transformer.InverseTransform(transform);

            Assert.AreEqual(BigInteger.One, inverseTransform[0]);
            Assert.AreEqual(new BigInteger(3), inverseTransform[1]);
            Assert.AreEqual(new BigInteger(1), inverseTransform[2]);
            Assert.AreEqual(BigInteger.Zero, inverseTransform[3]);
        }

        [Test]
        public void InvertOnePlusThreePlusOneUsingFFTBase257()
        {
            var transformer = new FastFermatFourierTransformer(3);
            var start = new[] {BigInteger.One, 3, 1};
            var fastTransform = transformer.Transform(start);
            var fastInverseTransform = transformer.InverseTransform(fastTransform);
            

            Assert.AreEqual(BigInteger.One, fastInverseTransform[0]);
            Assert.AreEqual(new BigInteger(3), fastInverseTransform[1]);
            Assert.AreEqual(new BigInteger(1), fastInverseTransform[2]);
            Assert.AreEqual(BigInteger.Zero, fastInverseTransform[3]);
        }


        [Test]
        public void SquareBinomial()
        {
            var transformer = new SlowFourierTransformer(5, 2, 4, 4);
            var transform = transformer.Transform(new[] { BigInteger.One, 1 });
            for (int i = 0; i < transform.Length; i++)
            {
                transform[i] = transform[i]*transform[i];
            }
            var inverseTransform = transformer.InverseTransform(transform);

            Assert.AreEqual(BigInteger.One, inverseTransform[0]);
            Assert.AreEqual(new BigInteger(2), inverseTransform[1]);
            Assert.AreEqual(new BigInteger(1), inverseTransform[2]);
            Assert.AreEqual(BigInteger.Zero, inverseTransform[3]);
        }

        [Test]
        public void TestAdditivityOfTransforms()
        {
            var transformer = new FastFermatFourierTransformer(3);
            var start1 = new[] { BigInteger.One, 3, 1 };
            var start2 = new[] { BigInteger.Zero, 5, 19, 4 };

            var transform1 = transformer.Transform(start1);
            var transform2 = transformer.Transform(start2);

            var sumTransform = new BigInteger[16];
            for (int i = 0; i < 16; i++)
            {
                sumTransform[i] = transform1[i] + transform2[i];
            }

            var inverseSumTransform = transformer.InverseTransform(sumTransform);

            var expectedSumTransform = new BigInteger[16];
            expectedSumTransform[0] = 1;
            expectedSumTransform[1] = 8;
            expectedSumTransform[2] = 20;
            expectedSumTransform[3] = 4;
            CollectionAssert.AreEqual(expectedSumTransform,inverseSumTransform);

        }

        [Test]
        public void TestWhatXToTheRMinusOneTransformsTo()
        {
            var transformer = new FastFermatFourierTransformer(3);
            var start = new[] {256, 0, 0, 0, 0, BigInteger.One};
            var transform = transformer.Transform(start);

            var start1 = new[] {0, 256, 0, 0, 0, 0, BigInteger.One};
            var transform1 = transformer.Transform(start1);

            var start2 = new[] { 0, 0, 256, 0, 0, 0, 0, BigInteger.One };
            var transform2 = transformer.Transform(start2);
        }

        [Test]
        public void TestTakingTransformOfXToTheROffIsStillEquivalentModR()
        {
            var transformer = new FastFermatFourierTransformer(3);
            var start = new[] {256, 0, 0, 0, 0, BigInteger.One};
            var transformOfStart = transformer.Transform(start);
            var xToTheR = new[] {BigInteger.MinusOne, 0, 0, 1};
            var transformOfXToTheR = transformer.Transform(xToTheR);
            var sumOfTransforms = new BigInteger[16];
            for (int i = 0; i < 16; i++)
            {
                sumOfTransforms[i] = (transformOfStart[i] + transformOfXToTheR[i])%257;

            }

            var inverseTransform = transformer.InverseTransform(sumOfTransforms);

            var reducedPolynomial = new Polynomial(inverseTransform).ReduceMod(257, 3);

            Assert.AreEqual(reducedPolynomial.CoefficientAt(0), new BigInteger(256));
            Assert.AreEqual(reducedPolynomial.CoefficientAt(1), BigInteger.Zero);
            Assert.AreEqual(reducedPolynomial.CoefficientAt(2), BigInteger.One);
            Assert.AreEqual(reducedPolynomial.Order(),2);
        }
     
        [Test]
        public void TestTakingTransformOfNOffIsStillEquivalentModR()
        {
            const int divisor = 24;
            var transformer = new FastFermatFourierTransformer(4);
            var start = new[] { 256, 0, 0, 0, 0, BigInteger.One };
            var transformOfStart = transformer.Transform(start);
            for (int j = 1; j < 16; j++) { 
                var n = new BigInteger[j];
                for (int k = 0; k < j - 1; k++)
                {
                    n[k] = 0;
                }
                n[j - 1] = divisor;
                var transformOfN = transformer.Transform(n);
                var sumOfTransforms = new BigInteger[32];
                for (int i = 0; i < 32; i++)
                {
                    sumOfTransforms[i] = ((transformOfStart[i] + transformOfN[i]) % 65537);
                }

                var inverseTransform = transformer.InverseTransform(sumOfTransforms);

                var reducedPolynomial = new Polynomial(inverseTransform).ReduceMod(divisor);

                Assert.AreEqual(reducedPolynomial.CoefficientAt(0), new BigInteger(16));
                Assert.AreEqual(reducedPolynomial.CoefficientAt(1), BigInteger.Zero);
                Assert.AreEqual(reducedPolynomial.CoefficientAt(2), BigInteger.Zero);
                Assert.AreEqual(reducedPolynomial.CoefficientAt(3), BigInteger.Zero);
                Assert.AreEqual(reducedPolynomial.CoefficientAt(4), BigInteger.Zero);
                Assert.AreEqual(reducedPolynomial.CoefficientAt(5), BigInteger.One);
            

                Assert.AreEqual(reducedPolynomial.Order(), 5);
            }
        }

       [Test]
       public void TestToByteArray()
       {
           var bi = new BigInteger(65538);
           var bytes = bi.ToByteArray();
           Assert.AreEqual(bytes[0],2);
           Assert.AreEqual(bytes[1],0);
           Assert.AreEqual(bytes[2],1);
       }

        [Test]
        [Category("Limits")]
        public void FourierTransformMultiplicationIsAccurate()
        {
            PrimeTester primeTester = new PrimeTester();
            BigInteger testNumber = 1500;
            BigInteger lastSuccess = 0,
                       lastFailure = 0,
                       untestedBetweenSuccessAndFailure = 0;

            while (lastFailure == 0 || untestedBetweenSuccessAndFailure > 0)
            {
                bool isPrime;
                try
                {
                    isPrime = primeTester.IsPrime(testNumber.ToString());
                }
                catch (FastComplexFourierTransformerException ex)
                {
                    if (lastFailure == testNumber) 
                    // have not found a failure in between last failure and last success
                    // so go back to test the untested numbers
                    {
                        testNumber = lastSuccess + 1;
                    }
                    else
                    {
                        testNumber = (lastSuccess + testNumber) / 2;
                        
                    }
                    lastFailure = testNumber;
                    untestedBetweenSuccessAndFailure = lastFailure - lastSuccess - 1;
                    continue;
                }
                if (isPrime)
                {
                    lastSuccess = testNumber;
                        
                    if (lastFailure == 0)
                    {
                        testNumber *= 2;
                    }
                    else
                    {
                        testNumber = (lastSuccess + lastFailure)/2;
                    }
                    untestedBetweenSuccessAndFailure = lastFailure - lastSuccess - 1;
                }
                else
                {

                    testNumber++;
                    
                    untestedBetweenSuccessAndFailure--;
                }
            }

            Assert.Greater(testNumber, new BigInteger(500000), "FourierTransform works up to 500000");
            Assert.Less(testNumber, new BigInteger(1000000), "Fourier Transform should definitely not be used after 1000000");
        }
    }
}
