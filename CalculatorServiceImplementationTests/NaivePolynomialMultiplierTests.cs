﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using CalculatorServiceImplementation.PolynomialMultipliers;
using NUnit.Framework;

namespace CalculatorServiceImplementationTests
{
    [TestFixture]
    
    public class NaivePolynomialMultiplierTests
    {
        [Test]
        public void TestSquaringBinomial()
        {
            BigInteger[] binomial = new BigInteger[] {1, 1};
            var result = new NaivePolynomialMultiplier().Multiply(binomial, binomial);

            CollectionAssert.AreEqual(result, new BigInteger[] {1, 2, 1});
        }
    }
}
