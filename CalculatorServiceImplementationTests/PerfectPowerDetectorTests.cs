﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CalculatorServiceInterfaces;
using NUnit.Framework;

namespace CalculatorServiceImplementationTests
{
    [TestFixture]
    public class PerfectPowerDetectorTests
    {
        [TestCase(2401)]
        [TestCase(4)]
        [TestCase(8)]
        [TestCase(32)]
        public void TestPerfectPower(int perfectPower)
        {
            Assert.IsTrue(new PerfectPowerDetector().IsPerfectPower(2401));
        }

        [TestCase(2400)]
        [TestCase(6)]
        [TestCase(3)]
        public void TestNonPerfectPower(int nonPerfectPower)
        {
            Assert.IsFalse(new PerfectPowerDetector().IsPerfectPower(nonPerfectPower));
        }

        [Test]
        public void TestGetsItWrongIfMaximumExponentIsToLow()
        {
            // 243 is 2^5 but we are only going to search up to 4th powers
            Assert.IsFalse(new PerfectPowerDetector().IsPerfectPower(243, 4));
        }

        [Test]
        public void TestAllNumbersUnder1Million()
        {
            const int topNumber = 1000000;
            int squareRoot = (int)Math.Sqrt(topNumber);
 
            bool[] perfectPower = new bool[topNumber + 1];

            for (int i = 2; i <= squareRoot; i++)
            {
                long power = i*i;
                while (power <= topNumber)
                {
                    perfectPower[power] = true;
                    power = power*i;
                }
            }

            var perfectPowerDetector = new PerfectPowerDetector();
            for (int i = 2; i < topNumber; i++)
            {
                Assert.AreEqual(perfectPower[i], perfectPowerDetector.IsPerfectPower(i), "{0} is a perfect power", i);
            }
        }
    }
}
