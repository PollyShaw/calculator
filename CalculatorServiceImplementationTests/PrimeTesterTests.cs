﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using CalculatorServiceImplementation;
using NUnit.Framework;

namespace CalculatorServiceImplementationTests
{
    [TestFixture]
    public class PrimeTesterTests
    {
        [Test]
        public void Test31() 
        {
            Assert.IsTrue(new PrimeTester().IsPrime("31"), "31 should be prime");
        }

        [Test]
        public void Test6()
        {
            Assert.IsFalse(new PrimeTester().IsPrime("6"), "6 should be composite");
        }

        [Test]
        public void Test561()
        {
            Assert.IsFalse(new PrimeTester().IsPrime("561"), "561 should be composite");
        }

        [Test]
        public void Test509()
        {
            Assert.IsTrue(new PrimeTester().IsPrime("509"), "509 should be prime");
        }

        [Test]
        public void Test6601()
        {
            Assert.IsFalse(new PrimeTester().IsPrime("6601"), "6601 should be composite");            
        }

        [Test]
        public void Test65537()
        {
            Assert.IsTrue(new PrimeTester().IsPrime("65537"), "65537 should be prime");
        }

        [Test]
        [Ignore("Takes too long")]
        public void TestM31()
        {
            var m31 = BigInteger.Pow(2, 31) - 1;
            Assert.IsTrue(new PrimeTester().IsPrime(m31.ToString()), "{0} should be prime", m31);
        }

        [Test]
        [Ignore("Takes too long")]
        public void Test100DigitPrime()
        {
            
            Assert.IsTrue(new PrimeTester().IsPrime("2074722246773485207821695222107608587480996474721117292752992589912196684750549658310084416732550077")); 
        }

        [Test]
        [Ignore("Takes too long")]
        public void Test100DigitComposite()
        {
            var composite = BigInteger.Parse("95647806479275528135733781266203904794419563064407")*
                            BigInteger.Parse("15452417011775787851951047309563159388840946309807");
            Assert.IsFalse(new PrimeTester().IsPrime(composite.ToString()));
        }

        [TestCase("36")]
        [TestCase("49")]
        [TestCase("243")]
        [TestCase("343")]
        [TestCase("2401")]
        [TestCase("256")]
        [TestCase("4")]
        public void TestPerfectPower(string power)
        {
            Assert.IsFalse(new PrimeTester().IsPrime(power));
        }

        [Test]
        public void TestSquaresLessThanA100Million()
        {
            for (var i = 2; i < 10000; i++)
            {
                Assert.IsFalse(new PrimeTester().IsPrime((i*i).ToString()));
            }
        }

        [Test]
        public void TestCubesLessThanABillion()
        {
        
            for (var i = 2; i < 1000; i++)
            {
                Assert.IsFalse(new PrimeTester().IsPrime((i*i*i).ToString()));
            }
        
        }
       
        [Test]
        public void Test3BeingGenerator()
        {
            for (int i = 4; i < 1024; i++)
            {
                BigInteger groupSize = BigInteger.Pow(2, i);
                BigInteger powModGroup = 3;
                for (int j = 0; j < i - 3; j++)
                {
                    powModGroup = (powModGroup*powModGroup)%groupSize;
                }
                Assert.AreNotEqual(powModGroup, BigInteger.One, "Index {0}",i);
                powModGroup = (powModGroup * powModGroup) % groupSize;
                Assert.AreEqual(powModGroup, BigInteger.One, "Index {0}", i);
                
            }
        }



        [Test]
        public void Test36()
        {
            Assert.IsFalse(new PrimeTester().IsPrime("36"));
        }

        [Test]
        public void TestAllPrimesLessThan100000()
        {
            var primes = new bool[100000];
           for (int i = 2; i < primes.Length; i++)
           {
               primes[i] = true;
           }

            primes[0] = false;
            primes[1] = false;
            var candidatePrime = 1;
            while (candidatePrime <= Math.Ceiling(Math.Sqrt(primes.Length)))
            {
                while (!primes[candidatePrime])
                {
                    candidatePrime++;
                }
                for (var multiple = 2 * candidatePrime; multiple < primes.Length; multiple += candidatePrime)
                {
                    primes[multiple] = false;
                }

                candidatePrime++;
            }
            List<int> falseComposites = new List<int>();
            List<int> falsePrimes = new List<int>();
            using (var fileWriter = new StreamWriter(File.OpenWrite("primes.txt")))
            {
                for (int i = 3; i < primes.Length; i++)
                {

                    if (primes[i])
                    {
                        if (!new PrimeTester().IsPrime(i.ToString()))
                        {
                            falseComposites.Add(i);
                        }
                    }
                    else
                    {
                        if (new PrimeTester().IsPrime(i.ToString()))
                        {
                            falsePrimes.Add(i);
                        }
                    }

                    if (primes[i])
                    {
                        fileWriter.WriteLine(i);
                    }
                    
                }
            }
            Assert.IsEmpty(falseComposites);
            Assert.IsEmpty(falsePrimes);
        }

        
    }
}
