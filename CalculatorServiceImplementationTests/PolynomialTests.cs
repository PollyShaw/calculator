﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using CalculatorServiceImplementation;
using CalculatorServiceImplementation.FourierTransformers;
using CalculatorServiceImplementation.PolynomialMultipliers;
using CalculatorServiceImplementation.ScalarMultipliers;
using NUnit.Framework;

namespace CalculatorServiceImplementationTests
{
    [TestFixture]
    [Category("Performance")]
    public class PolynomialTests
    {
        [Test]
        public void TestReduction()
        {
            var polynomial = new Polynomial(new BigInteger[] {BigInteger.One, BigInteger.One});

            var result = polynomial.ReduceMod(new Polynomial(BigInteger.Zero, BigInteger.One));

            Assert.AreEqual(1, polynomial.Order());

            Assert.AreEqual(0, result.Order());
            Assert.AreEqual(result.CoefficientAt(0), BigInteger.One);
        }

        [Test]
        public void TestReductionToZero()
        {
            var polynomial = new Polynomial(new BigInteger[] {BigInteger.One, BigInteger.One});

            var result = polynomial.ReduceMod(new Polynomial(BigInteger.One, BigInteger.One));

            Assert.AreEqual(0, result.Order());
            Assert.AreEqual(BigInteger.Zero, result.CoefficientAt(0));
        }

        [Test]
        public void TestReductionOfXPlus1Squared()
        {
            var polynomial = new Polynomial(new BigInteger[] {BigInteger.One, new BigInteger(2), BigInteger.One});

            var result = polynomial.ReduceMod(new Polynomial(BigInteger.One, BigInteger.One));

            Assert.AreEqual(0, result.Order());
            Assert.AreEqual(BigInteger.Zero, result.CoefficientAt(0));
        }

        [Test]
        public void TestReductionByXPlusOneSquared()
        {
            var polynomial =
                new Polynomial(new BigInteger[]
                    {new BigInteger(2), new BigInteger(4), new BigInteger(3), new BigInteger(1)});

            var result = polynomial.ReduceMod(new Polynomial(BigInteger.One, new BigInteger(2), BigInteger.One));

            Assert.AreEqual(1, result.Order());
            Assert.AreEqual(BigInteger.One, result.CoefficientAt(0));
            Assert.AreEqual(BigInteger.One, result.CoefficientAt(1));
        }

        [Test]
        public void TestReductionBy3()
        {
            var polynomial = new Polynomial(new BigInteger[]
                {
                    new BigInteger(1),
                    new BigInteger(3),
                    new BigInteger(3),
                    new BigInteger(1)
                });

            var result = polynomial.ReduceMod(new BigInteger(3));
            Assert.AreEqual(3, result.Order());
            Assert.AreEqual(BigInteger.One, result.CoefficientAt(0));
            Assert.AreEqual(BigInteger.Zero, result.CoefficientAt(1));
            Assert.AreEqual(BigInteger.Zero, result.CoefficientAt(2));
            Assert.AreEqual(BigInteger.One, result.CoefficientAt(3));


        }

        [Test]
        public void TestReductionByIntegerCanReduceOrder()
        {
            var polynomial = new Polynomial(new BigInteger[]
                {
                    new BigInteger(1),
                    new BigInteger(3),
                    new BigInteger(3),
                    new BigInteger(3)
                });

            var result = polynomial.ReduceMod(3);
            Assert.AreEqual(0, result.Order());
        }

        [Test]
        public void TestMultiplyXPlusOne()
        {
            var polynomial = new Polynomial(BigInteger.One, BigInteger.One);
            var result = polynomial.Times(polynomial);

            Assert.AreEqual(2, result.Order());

            Assert.AreEqual(BigInteger.One, result.CoefficientAt(0));
            Assert.AreEqual(new BigInteger(2), result.CoefficientAt(1));
            Assert.AreEqual(BigInteger.One, result.CoefficientAt(2));

        }

        [TestCase(10)]
        [TestCase(16)]
        [TestCase(17)]
        [TestCase(32)]
        [TestCase(64)]
        [TestCase(128)]
        [TestCase(256)]
        [TestCase(300)]
        [TestCase(500)]
        [TestCase(512)]
        [TestCase(513)]
        [TestCase(600)]
        [TestCase(1000)]
        [TestCase(1025)]
        [TestCase(2049)]
        [TestCase(4096)]
        [TestCase(4097)]
        public void CompareNaiveMultiplicationWithFFT(int orderOfPolynomial)
        {
            var coefficients = new BigInteger[orderOfPolynomial];
            for (int i = 0; i < orderOfPolynomial; i++)
            {
                coefficients[i] = i + 1;
            }

            var polynomial = new Polynomial(coefficients);
            int lgOrder = (int) Math.Ceiling(Math.Log(orderOfPolynomial, 2));

            var transformer = new FastFermatFourierTransformer(lgOrder);

            var multiplier = new FourierPolynomialMultiplier<BigInteger>(transformer, new BigIntegerScalarMultiplier());
            var now = DateTime.Now;
            for (int i = 0; i < 10; i++)
            {
                polynomial.Times(polynomial);
            }
            var naiveTimespan = DateTime.Now.Subtract(now);
            now = DateTime.Now;
            for (int i = 0; i < 10; i++)
            {
                polynomial.Times(polynomial, multiplier);
            }
            var fourierTimespan = DateTime.Now.Subtract(now);

            Debug.Print("For polynomials of order {0} naive takes {1:ss\\.fffff}s while fourier takes {2:ss\\.fffff}s",
                        orderOfPolynomial, naiveTimespan, fourierTimespan);

            Assert.Greater(naiveTimespan, fourierTimespan);

        }




        [TestCase(10)]
        [TestCase(16)]
        [TestCase(17)]
        [TestCase(32)]
        [TestCase(64)]
        [TestCase(80)]
        [TestCase(90)]
        [TestCase(95)]
        [TestCase(97)]
        [TestCase(99)]
        [TestCase(100)]
        [TestCase(128)]
        [TestCase(130)]
        [TestCase(256)]
        [TestCase(300)]
        [TestCase(500)]
        [TestCase(512)]
        [TestCase(513)]
        [TestCase(600)]
        [TestCase(1000)]
        [TestCase(1025)]
        [TestCase(2049)]
        [TestCase(4096)]
        [TestCase(4097)]
        [Category("Performance")]
        public void CompareNaiveMultiplicationWithComplexFFT(int orderOfPolynomial)
        {
            var coefficients = new BigInteger[orderOfPolynomial];
            for (int i = 0; i < orderOfPolynomial; i++)
            {
                coefficients[i] = i + 1;
            }

            var polynomial = new Polynomial(coefficients);
            int lgOrder = (int) Math.Ceiling(Math.Log(orderOfPolynomial, 2));

            var transformer = new FastComplexFourierTransformer(lgOrder + 1);

            var multiplier = new FourierPolynomialMultiplier<Complex>(transformer, new ComplexScalarMultiplier());
            var now = DateTime.Now;
            Polynomial naiveResult = null, fourierResult = null;
            for (int i = 0; i < 10; i++)
            {
                naiveResult = polynomial.Times(polynomial);
            }
            var naiveTimespan = DateTime.Now.Subtract(now);
            now = DateTime.Now;
            for (int i = 0; i < 10; i++)
            {
                fourierResult = polynomial.Times(polynomial, multiplier);
            }
            var fourierTimespan = DateTime.Now.Subtract(now);

            Debug.Print("For polynomials of order {0} naive takes {1:ss\\.fffff}s while fourier takes {2:ss\\.fffff}s",
                        orderOfPolynomial, naiveTimespan, fourierTimespan);

            Assert.AreEqual(naiveResult.Order(), fourierResult.Order());
            for (int i = 0; i <= naiveResult.Order(); i++)
            {
                Assert.AreEqual(naiveResult.CoefficientAt(i), fourierResult.CoefficientAt(i));
            }

                Assert.Greater(naiveTimespan, fourierTimespan);

        }

    }

}
