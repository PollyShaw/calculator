﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using CalculatorServiceImplementation;
using CalculatorServiceImplementation.Helpers;
using NUnit.Framework;

namespace CalculatorServiceImplementationTests
{
    [TestFixture]
    public class BigIntegerExtensionsTests
    {
        [Test]
        public void TestSmallNumbers()
        {
            for (int i = 0; i < 1000000; i++)
            {
                BigInteger number = new BigInteger(i);
                Assert.AreEqual(number.SquareRoot(), new BigInteger(Math.Floor(Math.Sqrt(i))), "Square root of {0}", i);
            }
        }

        [Test]
        public void TestCubeRootOfSmallNumbers()
        {
            var root = BigInteger.Zero;
            var cube = BigInteger.Zero;
            for (int i = 1; i < 1000000; i++)
            {

                if (i >= cube)
                {
                    root = root + 1;
                    cube = (root + 1) * (root + 1) * (root + 1);
                }

                BigInteger number = new BigInteger(i);
                Assert.AreEqual(root, number.NthRoot(3), "Cube root of {0}", i);
                
            }
        }

        [Test]
        public void TestCubeRootOfMediumSizedNumbers()
        {
            BigInteger baseNumber = BigInteger.Pow(new BigInteger(uint.MaxValue), 3);

            BigInteger expected = uint.MaxValue - 1;
            for (int i = 500000; i > 0; i--)
            {
                Assert.AreEqual(expected, (baseNumber - i).NthRoot(3), "subtracting {0}", i);
            }

            expected = uint.MaxValue;
            for (int i = 0; i < 500000; i++)
            {
                Assert.AreEqual(expected, (baseNumber + i).NthRoot(3), "adding {0}", i);
            }
        }

        [Test]
        public void TestMediumSizedNumbers()
        {
            BigInteger baseNumber = new BigInteger(ulong.MaxValue) + 1;

            BigInteger expected = uint.MaxValue;
            for (int i = 500000; i > 0; i--)
            {
                Assert.AreEqual(expected, (baseNumber - i).SquareRoot(), "subtracting {0}", i);
            }

            expected = new BigInteger(uint.MaxValue) + 1;             
            for (int i = 0; i < 500000; i++)
            {
                Assert.AreEqual(expected, (baseNumber + i).SquareRoot(), "adding {0}", i);
            }
        }

        [Test ]
        [Ignore("Takes ages just to calculate baseNumber")]
        public void TestQuiteBigNumbers()
        {
            BigInteger expected = BigInteger.Pow(2, int.MaxValue);
            BigInteger baseNumber = expected*expected;

            expected--;
            for (int i = 5000; i > 0; i--)
            {
                Assert.AreEqual(expected, (baseNumber - i).SquareRoot(), "subtracting {0}", i);
            }

            expected++;
            for (int i = 0; i < 5000; i++)
            {
                Assert.AreEqual(expected, (baseNumber + i).SquareRoot(), "adding {0}", i);
            }
        }
    }
}
